#Precipitation Raster Pseudocode 11/19/18
#Importing necessary modules

import arcpy
import os
import sys
import locale
import csv
import numpy as np

#Making sure the spatial analyst liscence has been checked out
arcpy.CheckExtension('spatial')
arcpy.CheckOutExtension('spatial')
#Defining classify
def classify:
    (set parameters)
    
#Setting variables
input_raster = ('input_raster')
definitions_file = ('definitions_file')
Additional_raster = ('additional_raster')

#Setting the exent of the area of interest
arcpy.env.extent = "Cache_valley_shapefile"
#Could also use extent class
arcyp.env.extent = (list_of_extents)

#Execute classification
classifiedraster = ClassifyRaster(input_raster, definitions_file, Additional_raster)

#Save locally
Classifiedraster.save('Filepath')

#Using a CSV reader/writer to get a list of precip values
def tableToCSV(input_table,FilePath_for_csv):
    Field List = arcpy.ListFields(input_table)
    Field Names = [name for field in list of fields)

#open CSV to which we will write the data
with open(FilePath_for_csv), 'wb') as csv_file:
        writer = csv.writer(csv_file)

#Specify output headers
writer.writerow(Field_names)  

#Closing the CSV
csv_file.close()