
#Importing needed modules
import sys
import arcpy
import os
import urllib2
import traceback

#Defining download parameters
def download(url):


    webFile = urllib2.urlopen(url)
    if url.endswith(".gif"):
            localFilePath = output_file
    else:
            localFilePath = os.path.splitext(out_file)[0] + ".gfw"
    arcpy.AddMessage("Downloading " + url + " to " + Localpath)
    localFile = open(localFilePath, 'wb')
    localFile.write(webFile.read())
    webFile.close()
    localFile.close()
    return localFilePath

try:
    #Reading tool parameters
    
    inp = arcpy.GetParameterAsText(0)
    out_file = arcpy.GetParameterAsText(1)
    out_folder = os.path.dirname(out_file)

    #Defining a list of Radar regions, this will be used to make URLs  
    region = {
        'National Mosaic': 'latest_radaronly',
        'Northern Rockies Sector': 'northrockies_radaronly',
        'Southern Rockies Sector': 'southrockies_radaronly',
        }

    #Download radar files.
    NWSUrl = r'http://radar.weather.gov/ridge/Conus/RadarImg/'
    NWSGfw = NWSUrl + region[inp] + '.gfw'
    NWSGif = NWSUrl + region[inp] + '.gif'
    download(NWSGfw)
    out_file = download(NWSGif)

    #Set NAD 1983 as the projection type
    if os.path.exists(os.path.join(arcpy.GetInstallInfo()['InstallDir'],
        "Coordinate Systems","Geographic Coordinate Systems","North America",
        "NAD 1983.prj")):
        prjFile = os.path.join(arcpy.GetInstallInfo()['InstallDir'],
        "Coordinate Systems", "Geographic Coordinate Systems","North America",
        "NAD 1983.prj")
        arcpy.DefineProjection_management(out_file, prjFile)
    else:
        sr = arcpy.SpatialReference("NAD 1983")
        arcpy.DefineProjection_management(out_file, sr)