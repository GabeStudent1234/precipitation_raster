# Precipitation_Raster
Author: Gabe Henry
Last Modified 12/4/18

A tool for retrieving web data regarding rainfall in millimeters, then classifying a surface raster based on those values.

Some data processing is required beforehand. Individual raster bands (red, green, blue) must be extracted and
made into seperate layer files for consumption by the tool.

Output locations must be specified for each classified image: choose a folder to store the outputs in,
then add a unique name to them after the last slash.

EXAMPLE: 
Red Band: F:\PYTHON\precipitation_raster\nws_precip_1day_20181130_geotiff\RED_BAND
Green Band: F:\PYTHON\precipitation_raster\nws_precip_1day_20181130_geotiff\GREEN_BAND
Blue Band: F:\PYTHON\precipitation_raster\nws_precip_1day_20181130_geotiff\BLUE_BAND

The workspace to be defined is the file path that holds the previously extracted red, green, and blue bands

At this time the search cursor that writes green band attribute table information to a CSV DOES NOT WORK,
however this function does work in the standalone script

There are two scripts, one outputs attribute information to a CSV (CLASSIFICATION_CSVOUT), and the other adds a new column to the attribute table(CLASSIFICATION_CURSOR). The latter does not work at this time.