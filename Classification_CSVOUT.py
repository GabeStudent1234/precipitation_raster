import arcpy
import os
import sys
import locale
import csv
from arcpy.sa import ClassifyRaster
from arcpy.sa import IsoClusterUnsupervisedClassification
arcpy.CheckOutExtension("Spatial")

# Set local variables
folder = r'F:\PYTHON\precipitation_raster'
arcpy.env.workspace = folder
os.chdir(folder)
arcpy.env.overwriteOutput = True

# Set local variables, since ISO Cluster Unsupervised classification only classifies one layer at a time
#This must be done for each layer
#For The Red Layer
inRaster1 = r'F:\PYTHON\precipitation_raster\Red_Band_1.lyr'
classes = 4
minMembers = 20
sampInterval = 10
outUnsupervised = IsoClusterUnsupervisedClassification(inRaster1, classes, minMembers, sampInterval)
outUnsupervised.save(r'F:\PYTHON\precipitation_raster\1DAYPRECIP_RED.tif')

#For the Green layer
inRaster2 = r'F:\PYTHON\precipitation_raster\Green_Band_2.lyr'
classes = 4
minMembers = 20
sampInterval = 10
outUnsupervised = IsoClusterUnsupervisedClassification(inRaster2, classes, minMembers, sampInterval)
outUnsupervised.save(r'F:\PYTHON\precipitation_raster\1DAYPRECIP_Green.tif')

#For the Blue layer
inRaster3 = r'F:\PYTHON\precipitation_raster\Blue_Band_3.lyr'
classes = 4
minMembers = 20
sampInterval = 10
outUnsupervised = IsoClusterUnsupervisedClassification(inRaster3, classes, minMembers, sampInterval)
outUnsupervised.save(r'F:\PYTHON\precipitation_raster\1DAYPRECIP_Blue.tif')

#Using a cursor to export data to a CSV
with arcpy.da.SearchCursor('1DAYPRECIP_Green.tif', field_names=['Value', 'Count'], 
                                 where_clause= '"Count" > 0') as searcher, open('11_30_18_PRECIP.csv', 'wb') as fp:
    writer = csv.writer(fp)
    writer.writerow(['Moisture_Level', 'Pixel_Count'])
    for row in searcher:
        writer.writerow(row)
    del searcher