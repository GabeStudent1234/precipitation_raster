import arcpy
import os
import sys
import locale
from arcpy.sa import IsoClusterUnsupervisedClassification
arcpy.CheckOutExtension("Spatial")


class Toolbox(object):
    def __init__(self):
        """Define the toolbox (the name of the toolbox is the name of the
        .pyt file)."""
        self.label = "Precipitation Classifier Toolbox"
        self.alias = "PR"

        # List of tool classes associated with this toolbox
        self.tools = [Classifier]


class Classifier(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "Precip Class"
        self.description = "Classify one hour precipitation rasters"
        self.canRunInBackground = False

    def getParameterInfo(self):
        """Define parameter definitions"""
        
        param0 = arcpy.Parameter(
        displayName = 'Precipitation Raster Layer 1',
        name = 'inRaster1',
        datatype = 'GPRasterLayer',
        parameterType = 'Not Required',
        direction = 'Input')
        
        param1 = arcpy.Parameter(
        displayName = 'Precipitation Raster Layer 2',
        name = 'inRaster2',
        datatype = 'GPRasterLayer',
        parameterType = 'Required',
        direction = 'Input')
        
        param2 = arcpy.Parameter(
        displayName = 'Precipitation Raster Layer 3',
        name = 'DEFolder3',
        datatype = 'GPRasterLayer',
        parameterType = 'Required',
        direction = 'Input')
        
        
        param3 = arcpy.Parameter(
        displayName = 'Define Workspace',
        name = 'Define_Workspace',
        datatype = 'DEWorkspace',
        parameterType = 'Required',
        direction = 'Input')
        
        
        param4 = arcpy.Parameter(
        displayName = 'Output Location 1',
        name = 'Output_Location_1',
        datatype = 'DEFolder',
        parameterType = 'Required',
        direction = 'Output')
        
        param5 = arcpy.Parameter(
        displayName = 'Output Location 2',
        name = 'Output_Location_2',
        datatype = 'DEFolder',
        parameterType = 'Required',
        direction = 'Output')
        
        param6 = arcpy.Parameter(
        displayName = 'Output Location 3',
        name = 'Output_Location_3',
        datatype = 'DEFolder',
        parameterType = 'Required',
        direction = 'Output')
        
        
        
        params = [param0, param1, param2, param3, param4, param5, param6]
        
        
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        return

    def execute(self, parameters, messages):
        """The source code of the tool."""
        
        
        #Get Parameters
        inRaster1 = parameters[0].valueAsText
        inRaster2 = parameters[1].valueAsText
        inRaster3 = parameters[2].valueAsText
        Define_Workspace = parameters[3].valueAsText
        Output_Location_1 = parameters[4].valueAsText
        Output_Location_2 = parameters[5].valueAsText
        Output_Location_3 = parameters[6].valueAsText
    
       
       
       
       # Set local variables
        arcpy.env.workspace = Define_Workspace
        
        arcpy.env.overwriteOutput = True
        
        # Set local variables, since ISO Cluster Unsupervised classification only classifies one layer at a time
        #This must be done for each layer
        #For The Red Layer
        inRaster1 = inRaster1
        classes = 4
        minMembers = 20
        sampInterval = 10
        outUnsupervised = IsoClusterUnsupervisedClassification(inRaster1, classes, minMembers, sampInterval)
        outUnsupervised.save(Output_Location_1)
        
        #For the Green layer
        inRaster2 = inRaster2
        classes = 4
        minMembers = 20
        sampInterval = 10
        outUnsupervised = IsoClusterUnsupervisedClassification(inRaster2, classes, minMembers, sampInterval)
        outUnsupervised.save(Output_Location_2)
        
        #For the Blue layer
        inRaster3 = inRaster3
        classes = 4
        minMembers = 20
        sampInterval = 10
        outUnsupervised = IsoClusterUnsupervisedClassification(inRaster3, classes, minMembers, sampInterval)
        outUnsupervised.save(Output_Location_3)
        
       #Using a cursor to export data to a CSV
        with arcpy.da.SearchCursor('Output_Location_2', field_names=['Value', 'Count'], 
                                 where_clause= '"Count" > 0') as searcher, open('11_30_18_PRECIP.csv', 'wb') as fp:
            writer = csv.writer(fp)
            writer.writerow(['Moisture_Level', 'Pixel_Count'])
            for row in searcher:
                writer.writerow(row)
                del searcher
        
        messages.addMessage('Very Cool!')
        return