import arcpy
import os
import sys
import locale
import csv
import numpy as np
from arcpy.sa import ClassifyRaster
from arcpy.sa import IsoClusterUnsupervisedClassification
arcpy.CheckOutExtension("Spatial")

# Set local variables
folder = r'F:\PYTHON\precipitation_raster'
arcpy.env.workspace = folder
os.chdir(folder)
arcpy.env.overwriteOutput = True

# Set local variables, since ISO Cluster Unsupervised classification only classifies one layer at a time
#This must be done for each layer
#For The Red Layer
inRaster1 = r'F:\PYTHON\precipitation_raster\Red_Band_1.lyr'
classes = 4
minMembers = 20
sampInterval = 10
outUnsupervised = IsoClusterUnsupervisedClassification(inRaster1, classes, minMembers, sampInterval)
outUnsupervised.save(r'F:\PYTHON\precipitation_raster\1DAYPRECIP_RED.tif')

#For the Green layer
inRaster2 = r'F:\PYTHON\precipitation_raster\Green_Band_2.lyr'
classes = 4
minMembers = 20
sampInterval = 10
outUnsupervised = IsoClusterUnsupervisedClassification(inRaster2, classes, minMembers, sampInterval)
outUnsupervised.save(r'F:\PYTHON\precipitation_raster\1DAYPRECIP_Green.tif')

#For the Blue layer
inRaster3 = r'F:\PYTHON\precipitation_raster\Blue_Band_3.lyr'
classes = 4
minMembers = 20
sampInterval = 10
outUnsupervised = IsoClusterUnsupervisedClassification(inRaster3, classes, minMembers, sampInterval)
outUnsupervised.save(r'F:\PYTHON\precipitation_raster\1DAYPRECIP_Blue.tif')

#Appending the Attribute table of 1DAYPRECIP_GREEN.tif
#Setting Variables
inFeatures = "1DAYPRECIP_Green.tif"
fieldName1 = "PRECIP_CLASS"
fieldPrecision = 11
fieldAlias = "Class"


#Executing Addfield for the two new fields
arcpy.AddField_management(inFeatures, fieldName1, "FLOAT", fieldPrecision,
                         field_alias=fieldAlias, field_is_nullable="NULLABLE")

#Using a cursor to add data to the newly created fields
in_table = "1DAYPRECIP_Green.tif"
Field = "PRECIP_CLA"
with arcpy.da.UpdateCursor(in_table, Field) as cursor:
    for row in cursor:
        print row
        